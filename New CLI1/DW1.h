#pragma once

namespace New_CLI1 
{

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

public ref class DW1 : public System::Windows::Forms::Form
{
public:
	DW1(void)
	{
		InitializeComponent();
	}
protected:
	~DW1()
	{
		if (components)
		{
			delete components;
		}
	}
private: System::Windows::Forms::Button^  button2;
private: System::Windows::Forms::Button^  button1;
private: System::Windows::Forms::Label^  label1;
private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code

void InitializeComponent(void)
{
	System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(DW1::typeid));
	this->button2 = (gcnew System::Windows::Forms::Button());
	this->button1 = (gcnew System::Windows::Forms::Button());
	this->label1 = (gcnew System::Windows::Forms::Label());
	this->SuspendLayout();
	// 
	// button2
	// 
	this->button2->BackColor = System::Drawing::Color::White;
	this->button2->Cursor = System::Windows::Forms::Cursors::Hand;
	resources->ApplyResources(this->button2, L"button2");
	this->button2->Name = L"button2";
	this->button2->UseVisualStyleBackColor = false;
	this->button2->Click += gcnew System::EventHandler(this, &DW1::button2_Click);
	// 
	// button1
	// 
	this->button1->BackColor = System::Drawing::Color::White;
	this->button1->Cursor = System::Windows::Forms::Cursors::Hand;
	resources->ApplyResources(this->button1, L"button1");
	this->button1->Name = L"button1";
	this->button1->UseVisualStyleBackColor = false;
	this->button1->Click += gcnew System::EventHandler(this, &DW1::button1_Click);
	// 
	// label1
	// 
	this->label1->BackColor = System::Drawing::Color::WhiteSmoke;
	resources->ApplyResources(this->label1, L"label1");
	this->label1->Name = L"label1";
	// 
	// DW1
	// 
	resources->ApplyResources(this, L"$this");
	this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
	this->AutoValidate = System::Windows::Forms::AutoValidate::Disable;
	this->BackColor = System::Drawing::Color::WhiteSmoke;
	this->ControlBox = false;
	this->Controls->Add(this->button2);
	this->Controls->Add(this->button1);
	this->Controls->Add(this->label1);
	this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
	this->MaximizeBox = false;
	this->MinimizeBox = false;
	this->Name = L"DW1";
	this->ResumeLayout(false);
}

#pragma endregion

private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e)
{
	this->DialogResult = System::Windows::Forms::DialogResult::OK;
	this->Close();
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e)
{
	this->DialogResult = System::Windows::Forms::DialogResult::Cancel;
	this->Close();
}
};
}
