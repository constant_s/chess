#include "Field.h"

Field::Field()
{
	if (i != 2)
	{
		wb = gcnew SolidBrush(Color::White);
		bb = gcnew SolidBrush(Color::Gray);
		wf = gcnew SolidBrush(Color::Black);
		bf = gcnew SolidBrush(Color::Black);
		con = gcnew SolidBrush(Color::Red);
	}
	g.SetStats(0, 0, 2);
	p00.SetStats(0, 1, 0);
	p01.SetStats(1, 1, 0);
	p02.SetStats(2, 1, 0);
	p03.SetStats(3, 1, 0);
	p04.SetStats(4, 1, 0);
	p05.SetStats(5, 1, 0);
	p06.SetStats(6, 1, 0);
	p07.SetStats(7, 1, 0);
	p10.SetStats(0, 6, 1);
	p11.SetStats(1, 6, 1);
	p12.SetStats(2, 6, 1);
	p13.SetStats(3, 6, 1);
	p14.SetStats(4, 6, 1);
	p15.SetStats(5, 6, 1);
	p16.SetStats(6, 6, 1);
	p17.SetStats(7, 6, 1);
	h01.SetStats(1, 0, 0);
	h02.SetStats(6, 0, 0);
	h11.SetStats(1, 7, 1);
	h12.SetStats(6, 7, 1);
	r00.SetStats(0, 0, 0);
	r01.SetStats(7, 0, 0);
	r10.SetStats(0, 7, 1);
	r11.SetStats(7, 7, 1);
	b01.SetStats(2, 0, 0);
	b02.SetStats(5, 0, 0);
	b11.SetStats(2, 7, 1);
	b12.SetStats(5, 7, 1);
	q0.SetStats(3, 0, 0);
	q1.SetStats(3, 7, 1);
	k0.SetStats(4, 0, 0);
	k1.SetStats(4, 7, 1);
	field = gcnew array<array<Figure^>^>(8);
	for (int i = 0; i < 8; i++)
	{
		field[i] = gcnew array<Figure^>(8);
		for (int j = 0; j < 8; j++)
		{
			field[i][j] = gcnew Figure;
		}
	}
	s = nullptr;
	i = 1;
	v = 1;
	win = 2;
	Entry();
}

void Field::operator=(Field^ f)
{
	g = f->g;
	p00 = f->p00;
	p01 = f->p01;
	p02 = f->p02;
	p03 = f->p03;
	p04 = f->p04;
	p05 = f->p05;
	p06 = f->p06;
	p07 = f->p07;
	p10 = f->p10;
	p11 = f->p11;
	p12 = f->p12;
	p13 = f->p13;
	p14 = f->p14;
	p15 = f->p15;
	p16 = f->p16;
	p17 = f->p17;
	h01 = f->h01;
	h02 = f->h02;
	h11 = f->h11;
	h12 = f->h12;
	r00 = f->r00;
	r01 = f->r01;
	r10 = f->r10;
	r11 = f->r11;
	b01 = f->b01;
	b02 = f->b02;
	b11 = f->b11;
	b12 = f->b12;
	q0 = f->q0;
	q1 = f->q1;
	k0 = f->k0;
	k1 = f->k1;
	field = gcnew array<array<Figure^>^>(8);
	for (int i = 0; i < 8; i++)
	{
		field[i] = gcnew array<Figure^>(8);
		for (int j = 0; j < 8; j++)
		{
			field[i][j] = gcnew Figure;
		}
	}
	v = f->v;
	win = f->win;
	s = gcnew System::String(f->s);
	Entry();
}

void Field::Entry()
{
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			field[i][j] = gcnew Figure(g);
	Entry(%p00);
	Entry(%p01);
	Entry(%p02);
	Entry(%p03);
	Entry(%p04);
	Entry(%p05);
	Entry(%p06);
	Entry(%p07);
	Entry(%p10);
	Entry(%p11);
	Entry(%p12);
	Entry(%p13);
	Entry(%p14);
	Entry(%p15);
	Entry(%p16);
	Entry(%p17);
	Entry(%h01);
	Entry(%h02);
	Entry(%h11);
	Entry(%h12);
	Entry(%r00);
	Entry(%r01);
	Entry(%r10);
	Entry(%r11);
	Entry(%b01);
	Entry(%b02);
	Entry(%b11);
	Entry(%b12);
	Entry(%q0);
	Entry(%q1);
	Entry(%k0);
	Entry(%k1);
}
void Field::Entry(Figure ^f)
{
	if (f->GetColor() != 2)
		field[f->GetCoordX()][f->GetCoordY()] = f;
}
void Field::OutField(Graphics^ g)
{
	g->Clear(System::Drawing::Color::WhiteSmoke);
	System::Drawing::Pen^ t = gcnew System::Drawing::Pen(System::Drawing::Color::Black);
	t->Width = 50;
	if (v % 2 == 1)
		g->DrawString("      A      B      C      D      E      F      G      H", gcnew System::Drawing::Font("Arial", 16), Brushes::Black, 0, 0);
	else
		g->DrawString("      H      G      F      E      D      C      B      A", gcnew System::Drawing::Font("Arial", 16), Brushes::Black, 0, 0);
	for (int i = 0; i <8; i++)
	{
		
		System::String^ str;
		str = nullptr;
		str += wchar_t((char*)56-i);
		g->DrawString(str, gcnew System::Drawing::Font("Arial", 16), Brushes::Black, 0, (v % 2) == 1 ? (i * 50 + 40) : ((7 - i) * 50 + 40));
		for (int j = 0; j < 8; j++)
			g->FillRectangle((i + j) % 2 == 0 ? wb : bb, j * 50 + 25, i * 50 + 20, 50, 50);
		g->DrawString(str, gcnew System::Drawing::Font("Arial", 16), Brushes::Black, 425, (v % 2) == 1 ? (i * 50 + 40) : ((7 - i) * 50 + 40));
	}
	if (v % 2 == 1)
		g->DrawString("      A      B      C      D      E      F      G      H", gcnew System::Drawing::Font("Arial", 16), Brushes::Black, 0, 425);
	else
		g->DrawString("      H      G      F      E      D      C      B      A", gcnew System::Drawing::Font("Arial", 16), Brushes::Black, 0, 425);
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			field[j][i]->Output(v % 2, g, wf, bf);
}
int Field::Move(int x1, int y1, int x2, int y2, TextBox^ t)
{ 
	Figure^ f = %g;
	Figure^ r = %g;
	int m = v % 2;
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			if (field[i][j]->GetFig() == "Pawn" && field[i][j]->GetColor() == m && field[i][j]->GetCas() == 1)
				field[i][j]->SetCas();
	if (field[x1][y1]->GetColor() == 2 || field[x1][y1]->GetColor() != m || field[x2][y2]->GetColor() == m)
	{
		t->Text += "Error!\r\n";
		return 1;
	}
	f = field[x2][y2];
	r = field[x1][y1];
	field[x1][y1]->Motion(x2, y2, field);
	if (field[x1][y1]->GetCoordX() == x1 && field[x1][y1]->GetCoordY() == y1)
	{
		t->Text += "Error!\r\n";
		return 1;
	}
	field[x2][y2]->Death();
	Entry();
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			if (field[i][j]->GetColor() != m && field[i][j]->CheckCheck(field) == 1)
			{

				r->SetStats(x1, y1, m);
				f->SetStats(x2, y2, 1 - m);
				if (f->GetFig() == "Pawn" && f->GetCas() > 10)
					f->Replacement();
				field[x2][y2] = f;
				t->Text += "Error!\r\n";
				Entry();
				return 1;
			}
			else
				for (int i = 0; i < 8; i++)
					for (int j = 0; j < 8; j++)
						if (field[i][j]->GetFig() == "King" && field[i][j]->GetColor() == m)
							field[i][j]->SetCheck(0);
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			if (field[i][j]->GetColor() == m && field[i][j]->CheckCheck(field) == 1)
				s += "Chech!\t\t\t\n";
	field[x2][y2] = f;
	if (field[x2][y2]->GetColor() != m)
		field[x2][y2]->Death();
	Entry();
	v++;
	return Patta();
}
void Field::About(Graphics^ g)
{
	g->DrawString("  C       H       E       S        S", gcnew System::Drawing::Font("FreeSerif", 24), Brushes::Green, 0, 125);
	g->DrawString("�����: �������� ����������", gcnew System::Drawing::Font("FreeSerif", 18), Brushes::DarkMagenta, 70, 210);
	g->DrawString("2015", gcnew System::Drawing::Font("FreeSerif", 16), Brushes::DarkGreen, 40, 340);

}
void Field::Win(Graphics^ g)
{
	if (win == 3)
		g->DrawString("��������� ������: �����!", gcnew System::Drawing::Font("FreeSerif", 24), Brushes::DarkBlue, 5, 125);
	else
	{
		g->DrawString("�����������!", gcnew System::Drawing::Font("FreeSerif", 24), Brushes::Green, 75, 25);
		if (win == 0)
			g->DrawString("������ ������� ����� 2", gcnew System::Drawing::Font("FreeSerif", 24), Brushes::DarkBlue, 5, 125);
		else
			g->DrawString("������ ������� ����� 1", gcnew System::Drawing::Font("FreeSerif", 24), Brushes::DarkRed, 5, 125);
		g->DrawString("������� �� ����!", gcnew System::Drawing::Font("FreeSerif", 24), Brushes::Green, 60, 225);
	}
}
System::String^ Field::GetFigure(int x, int y)
{
	return field[x][y]->GetFig();
}
void Field::SetI(int j)
{
	i = j;
}
int Field::GetI()
{
	return i;
}
int Field::GetV()
{
	return v;
}
void Field::SaveToFile(System::Object ^obj, System::String^ s)
{
	System::Runtime::Serialization::Formatters::Binary::BinaryFormatter^ binF = gcnew System::Runtime::Serialization::Formatters::Binary::BinaryFormatter ();
	System::IO::FileStream^ fStream = gcnew System::IO::FileStream(s, FileMode::Create, FileAccess::Write, FileShare::None);
	fStream->Position = 0;
	binF->Serialize(fStream, obj);
	fStream->Close();
}
Field^ Field::LoadFromFile(System::String^ s)
{
	System::Runtime::Serialization::Formatters::Binary::BinaryFormatter^ binF = gcnew System::Runtime::Serialization::Formatters::Binary::BinaryFormatter();
	System::IO::FileStream^ fStream = gcnew System::IO::FileStream(s, FileMode::Open, FileAccess::Read, FileShare::None);
	fStream->Position = 0;
	Field^ f = (Field^)binF->Deserialize(fStream);
	fStream->Close();
	return f;
}
void Field::SetWin(int w)
{
	win = w;
}
int Field::GetWin()
{
	return win;
}
Field::~Field()
{
	delete field;
}
void Field::SetString(System::String^ j)
{
	s += j;
}
System::String^ Field::GetString()
{
	return s;
}
void Field::SetColors(SolidBrush^ a, SolidBrush^ b, SolidBrush^ e, SolidBrush^ d, SolidBrush^ c)
{
	wb = a;
	bb = b;
	wf = e;
	bf = d;
	con = c;
}
SolidBrush^ Field::GetCon()
{
	return con;
}
int Field::GetColor(int x, int y)
{
	return field[x][y]->GetColor();
}
int Field::Patta()
{
	int l = 0;
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			if (field[i][j]->GetColor() == v % 2)
				l += field[i][j]->MoveCheck(field) == 1;	
	if (l == 0)
	{
		for (int i = 0; i < 8; i++)
			for (int j = 0; j < 8; j++)
				if (field[i][j]->GetColor() == v % 2 && field[i][j]->GetFig() == "King" && field[i][j]->GetCheck() == 1)
				{
					s += "Checkmate!\t\t\n";
					SetWin((v - 1) % 2);
					return 2;
				}
		s += "Patta!\t\t\n";
		SetWin(3);
		return 3;
	}
	return 0;
}