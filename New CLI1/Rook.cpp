﻿#include "Rook.h"

Rook::Rook() : Figure()
{
	cas = 0;
}
Rook::Rook(Rook% r)
{
	mas[0] = r.mas[0];
	mas[1] = r.mas[1];
	cas = r.cas;
	color = r.color;
}

void Rook::operator=(Rook%r)
{
	mas[0] = r.mas[0];
	mas[1] = r.mas[1];
	cas = r.cas;
	color = r.color;
}

void Rook::Motion(int x, int y, array<array<Figure^>^>^ f)
{
	int l = 0;
	if (x >= 0 && y >= 0 && x <= 8 && y <= 8)
	{
		if (y == mas[1])
		{
			if (x < mas[0]) 
				for (int i = mas[0] - 1; i > x; i--)
					if (f[i][mas[1]]->GetColor() != 2)
						l++;
			if (x > mas[0])
				for (int i = mas[0] + 1; i < x; i++)
					if (f[i][mas[1]]->GetColor() != 2)
						l++;
			if (l == 0)
			{
				mas[0] = x;
				mas[1] = y;
				cas++;
				return;
			}
		}
		if (x == mas[0])
		{
			if (y < mas[1])
				for (int i = mas[1] - 1; i > y; i--)
					if (f[mas[0]][i]->GetColor() != 2)
						l++;
			if (y > mas[1])
				for (int i = mas[1] + 1; i < y; i++)
					if (f[mas[0]][i]->GetColor() != 2)
						l++;
			if (l == 0)
			{
				mas[0] = x;
				mas[1] = y;
				cas++;
				return;
			}
		}
	}
}
void Rook::Output(int m, Graphics^ g, SolidBrush^ w, SolidBrush^ b)
{
	g->DrawString(color == 1 ? L"♖" : L"♜", gcnew System::Drawing::Font("Arial Unicode MS", 26), color == 1 ? w : b, m == 1 ? (mas[0] * 50 + 26) : ((7 - mas[0]) * 50 + 26), m == 1 ? (mas[1] * 50 + 25) : ((7 - mas[1]) * 50 + 25));
}
int Rook::GetColor()
{
	return color;
}
int Rook::GetCoordX()
{
	return mas[0];
}
int Rook::GetCoordY()
{
	return mas[1];
}
int Rook::Death()
{
	color = 2;
	return 1;
}
int Rook::GetCas()
{
	return cas;
}
System::String^ Rook::GetFig()
{
	return "Rook";
}
int Rook::CheckCheck(array<array<Figure^>^>^ f)
{
	for (int i = 0; i < 8; i++)
	{
		if (f[mas[0]][i]->GetFig() == "King" && f[mas[0]][i]->GetColor() != color)
		{
			int l = 0;
			if (i < mas[1])
				for (int j = mas[1] - 1; j > i; j--)
					if (f[mas[0]][j]->GetColor() != 2)
						l++;
			if (i > mas[1])
				for (int j = mas[1] + 1; j < i; j++)
					if (f[mas[0]][j]->GetColor() != 2)
						l++;
			if (l == 0)
			{
				f[mas[0]][i]->SetCheck(1);
				return 1;
			}
		}
		if (f[i][mas[1]]->GetFig() == "King" && f[i][mas[1]]->GetColor() != color)
		{
			int l = 0;
			if (i < mas[0])
				for (int j = mas[0] - 1; j > i; j--)
					if (f[j][mas[1]]->GetColor() != 2)
						l++;
			if (i > mas[0])
				for (int j = mas[0] + 1; j < i; j++)
					if (f[j][mas[1]]->GetColor() != 2)
						l++;
			if (l == 0)
			{
				f[i][mas[1]]->SetCheck(1);
				return 1;
			}
		}
	}
	return 0;
}
void Rook::SetStats(int x, int y, int c)
{
	if (x >= 0 && y >= 0 && x <= 8 && y <= 8 && (c == 0 || c == 1))
	{
		mas[0] = x;
		mas[1] = y;
		color = c;
	}
}
int Rook::MoveCheck(array<array<Figure^>^>^ f)
{
	for (int i = 0; i < 8; i++)
	{
		if (i < mas[1])
			for (int j = mas[1] - 1; j >= i; j--)
				if (f[mas[0]][j]->GetColor() != color)
					if (Figure::MovingCheck(mas[0], mas[1], mas[0], j, f) == 1)
						return 1;
		if (i > mas[1])
			for (int j = mas[1] + 1; j <= i; j++)
				if (f[mas[0]][j]->GetColor() != color)
					if (Figure::MovingCheck(mas[0], mas[1], mas[0], j, f) == 1)
						return 1;
		if (i < mas[0])
			for (int j = mas[0] - 1; j >= i; j--)
				if (f[j][mas[1]]->GetColor() != color)
					if (Figure::MovingCheck(mas[0], mas[1], j, mas[1], f) == 1)
						return 1;
		if (i > mas[0])
			for (int j = mas[0] + 1; j <= i; j++)
				if (f[j][mas[1]]->GetColor() != color)
					if (Figure::MovingCheck(mas[0], mas[1], j, mas[1], f) == 1)
						return 1;
	}
	return 0;
}