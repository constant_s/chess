﻿#pragma once

namespace New_CLI1 
{

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

public ref class DW6 : public System::Windows::Forms::Form
{
public:
	DW6(int c)
	{
		color = c;
		InitializeComponent();
	}
protected:
	~DW6()
	{
		if (components)
		{
			delete components;
		}
	}
private: System::Windows::Forms::Button^  button1;
private: System::Windows::Forms::Button^  button2;
private: System::Windows::Forms::Button^  button3;
private: System::Windows::Forms::Button^  button4;
private: int color;
private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code

void InitializeComponent(void)
{
	this->button1 = (gcnew System::Windows::Forms::Button());
	this->button2 = (gcnew System::Windows::Forms::Button());
	this->button3 = (gcnew System::Windows::Forms::Button());
	this->button4 = (gcnew System::Windows::Forms::Button());
	this->SuspendLayout();
	// 
	// button1
	// 
	this->button1->Font = (gcnew System::Drawing::Font(L"Arial Unicode MS", 36, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button1->Location = System::Drawing::Point(14, 14);
	this->button1->Name = L"button1";
	this->button1->Size = System::Drawing::Size(84, 84);
	this->button1->TabIndex = 0;
	this->button1->Text = L" ";
	this->button1->UseVisualStyleBackColor = true;
	this->button1->Click += gcnew System::EventHandler(this, &DW6::button1_Click);
	// 
	// button2
	// 
	this->button2->Font = (gcnew System::Drawing::Font(L"Arial Unicode MS", 36, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button2->Location = System::Drawing::Point(105, 14);
	this->button2->Name = L"button2";
	this->button2->Size = System::Drawing::Size(84, 84);
	this->button2->TabIndex = 1;
	this->button2->Text = L" ";
	this->button2->UseVisualStyleBackColor = true;
	this->button2->Click += gcnew System::EventHandler(this, &DW6::button2_Click);
	// 
	// button3
	// 
	this->button3->Font = (gcnew System::Drawing::Font(L"Arial Unicode MS", 36, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button3->Location = System::Drawing::Point(196, 14);
	this->button3->Name = L"button3";
	this->button3->Size = System::Drawing::Size(84, 84);
	this->button3->TabIndex = 2;
	this->button3->Text = L" ";
	this->button3->UseVisualStyleBackColor = true;
	this->button3->Click += gcnew System::EventHandler(this, &DW6::button3_Click);
	// 
	// button4
	// 
	this->button4->Font = (gcnew System::Drawing::Font(L"Arial Unicode MS", 36, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button4->Location = System::Drawing::Point(287, 14);
	this->button4->Name = L"button4";
	this->button4->Size = System::Drawing::Size(84, 84);
	this->button4->TabIndex = 3;
	this->button4->Text = L" ";
	this->button4->UseVisualStyleBackColor = true;
	this->button4->Click += gcnew System::EventHandler(this, &DW6::button4_Click);
	// 
	// DW6
	// 
	this->AutoScaleDimensions = System::Drawing::SizeF(9, 18);
	this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
	this->BackColor = System::Drawing::Color::WhiteSmoke;
	this->ClientSize = System::Drawing::Size(382, 107);
	this->ControlBox = false;
	this->Controls->Add(this->button4);
	this->Controls->Add(this->button3);
	this->Controls->Add(this->button2);
	this->Controls->Add(this->button1);
	this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
	this->MaximizeBox = false;
	this->MinimizeBox = false;
	this->Name = L"DW6";
	this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
	this->Text = L"Выберите фигуру";
	this->Load += gcnew System::EventHandler(this, &DW6::DW6_Load);
	this->ResumeLayout(false);
}

#pragma endregion

private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->DialogResult = System::Windows::Forms::DialogResult::OK;
	this->Close();
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->DialogResult = System::Windows::Forms::DialogResult::Yes;
	this->Close();
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->DialogResult = System::Windows::Forms::DialogResult::No;
	this->Close();
}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e)
{
	this->DialogResult = System::Windows::Forms::DialogResult::Cancel;
	this->Close();
}
private: System::Void DW6_Load(System::Object^  sender, System::EventArgs^  e) 
{
	if (color == 1)
		button1->Text = L"♕\r";
	else button1->Text = L"♛\r";
	if (color == 1)
		button2->Text = L"♗\r";
	else button2->Text = L"♝\r";
	if (color == 1)
		button3->Text = L"♘\r";
	else button3->Text = L"♞\r";
	if (color == 1)
		button4->Text = L"♖\r";
	else button4->Text = L"♜\r";
}
};
}
