﻿#include "Bishop.h"

Bishop::Bishop() : Figure()
{
}
Bishop::Bishop(Bishop% b)
{
	mas[0] = b.mas[0];
	mas[1] = b.mas[1];
	color = b.color;
}

void Bishop::operator=(Bishop%b)
{
	mas[0] = b.mas[0];
	mas[1] = b.mas[1];
	color = b.color;
}

void Bishop::Motion(int x, int y, array<array<Figure^>^>^f)
{
	int l = 0;
	if (x >= 0 && y >= 0 && x <= 8 && y <= 8)
	{
		for (int i = 0; i < 8; i++)
		{
			if (x == mas[0] + i && y == mas[1] + i) 
			{
				for (int j = mas[0] + 1, k = mas[1] + 1; j < x, k < y; j++, k++)
					if (f[j][k]->GetColor() != 2)
						l++;
				if (l == 0)
				{
					mas[0] = x;
					mas[1] = y;
					return;
				}
			}
			if (x == mas[0] - i && y == mas[1] - i) 
			{
				for (int j = mas[0] - 1, k = mas[1] - 1; j > x, k > y; j--, k--)
					if (f[j][k]->GetColor() != 2)
						l++;
				if (l == 0)
				{
					mas[0] = x;
					mas[1] = y;
					return;
				}
			}
			if (x == mas[0] + i && y == mas[1] - i)
			{
				for (int j = mas[0] + 1, k = mas[1] - 1; j < x, k > y; j++, k--)
					if (f[j][k]->GetColor() != 2)
						l++;
				if (l == 0)
				{
					mas[0] = x;
					mas[1] = y;
					return;
				}
			}
			if (x == mas[0] - i && y == mas[1] + i)
			{
				for (int j = mas[0] - 1, k = mas[1] + 1; j > x, k < y; j--, k++)
					if (f[j][k]->GetColor() != 2)
						l++;
				if (l == 0)
				{
					mas[0] = x;
					mas[1] = y;
					return;
				}
			}
		}
	}
}
void Bishop::Output(int m, Graphics^ g, SolidBrush^ w, SolidBrush^ b)
{
	g->DrawString(color == 1 ? L"♗" : L"♝", gcnew System::Drawing::Font("Arial Unicode MS", 26), color == 1 ? w : b, m == 1 ? (mas[0] * 50 + 26) : ((7 - mas[0]) * 50 + 26), m == 1 ? (mas[1] * 50 + 25) : ((7 - mas[1]) * 50 + 25));
}
int Bishop::GetColor()
{
	return color;
}
int Bishop::GetCoordX()
{
	return mas[0];
}
int Bishop::GetCoordY()
{
	return mas[1];
}
int Bishop::Death()
{
	color = 2;
	return 1;
}
System::String^ Bishop::GetFig()
{
	return "Bishop";
}
int Bishop::CheckCheck(array<array<Figure^>^>^ f)
{
	for (int i = 0; i < 8; i++)
	{
		int l = 0;
		if (mas[0] + i <= 7 && mas[1] + i <= 7)
			if (f[mas[0] + i][mas[1] + i]->GetFig() == "King" && f[mas[0] + i][mas[1] + i]->GetColor() != color)
			{
			for (int j = mas[0] + 1, k = mas[1] + 1; j < mas[0] + i, k < mas[1] + i; j++, k++)
					if (f[j][k]->GetColor() != 2)
						l++;
				if (l == 0)
				{
					f[mas[0] + i][mas[1] + i]->SetCheck(1);
					return 1;
				}
			}
		if (mas[0] - i >= 0 && mas[1] - i >= 0)
			if (f[mas[0] - i][mas[1] - i]->GetFig() == "King" && f[mas[0] - i][mas[1] - i]->GetColor() != color)
			{
			for (int j = mas[0] - 1, k = mas[1] - 1; j > mas[0] - i, k > mas[1] - i; j--, k--)
					if (f[j][k]->GetColor() != 2)
						l++;
				if (l == 0)
				{
					f[mas[0] - i][mas[1] - i]->SetCheck(1);
					return 1;
				}
			}
		if (mas[0] + i <= 7 && mas[1] - i >= 0)
			if (f[mas[0] + i][mas[1] - i]->GetFig() == "King" && f[mas[0] + i][mas[1] - i]->GetColor() != color)
			{
			for (int j = mas[0] + 1, k = mas[1] - 1; j < mas[0] + i, k > mas[1] - i; j++, k--)
					if (f[j][k]->GetColor() != 2)
						l++;
				if (l == 0)
				{
					f[mas[0] + i][mas[1] - i]->SetCheck(1);
					return 1;
				}
			}
		if (mas[0] - i >= 0 && mas[1] + i <= 7)
			if (f[mas[0] - i][mas[1] + i]->GetFig() == "King" && f[mas[0] - i][mas[1] + i]->GetColor() != color)
			{
			for (int j = mas[0] - 1, k = mas[1] + 1; j > mas[0] - i, k < mas[1] + i; j--, k++)
					if (f[j][k]->GetColor() != 2)
						l++;
				if (l == 0)
				{
					f[mas[0] - i][mas[1] + i]->SetCheck(1);
					return 1;
				}
			}
	}
	return 0;
}
void Bishop::SetStats(int x, int y, int c)
{
	if (x >= 0 && y >= 0 && x <= 8 && y <= 8 && (c == 0 || c == 1))
	{
		mas[0] = x;
		mas[1] = y;
		color = c;
	}
}
int Bishop::MoveCheck(array<array<Figure^>^>^ f)
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = mas[0] + 1, k = mas[1] + 1; j <= mas[0] + i, k <= mas[1] + i; j++, k++)
			if (j < 8 && k < 8 && f[j][k]->GetColor() != color)
				if (Figure::MovingCheck(mas[0], mas[1], j, k, f) == 1)
					return 1;
		for (int j = mas[0] - 1, k = mas[1] - 1; j >= mas[0] - i, k >= mas[1] - i; j--, k--)
			if (j >= 0 && k >= 0 && f[j][k]->GetColor() != color)
				if (Figure::MovingCheck(mas[0], mas[1], j, k, f) == 1)
					return 1;
		for (int j = mas[0] + 1, k = mas[1] - 1; j <= mas[0] + i, k >= mas[1] - i; j++, k--)
			if (j < 8 && k >= 0 && f[j][k]->GetColor() != color)
				if (Figure::MovingCheck(mas[0], mas[1], j, k, f) == 1)
					return 1;
		for (int j = mas[0] - 1, k = mas[1] + 1; j >= mas[0] - i, k <= mas[1] + i; j--, k++)
			if (j >= 0 && k < 8 && f[j][k]->GetColor() != color)
				if (Figure::MovingCheck(mas[0], mas[1], j, k, f) == 1)
					return 1;
	}
	return 0;
}