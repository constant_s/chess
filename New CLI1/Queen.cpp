﻿#include "Queen.h"

Queen::Queen() : Figure()
{
}
Queen::Queen(Queen% q)
{
	mas[0] = q.mas[0];
	mas[1] = q.mas[1];
	color = q.color;
}

void Queen::operator=(Queen%q)
{
	mas[0] = q.mas[0];
	mas[1] = q.mas[1];
	color = q.color;
}

void Queen::Motion(int x, int y, array<array<Figure^>^>^ f)
{
	int l = 0;
	if (x >= 0 && y >= 0 && x <= 8 && y <= 8)
	{
		for (int i = 0; i < 8; i++)
		{
			if (x == mas[0] + i && y == mas[1] + i)
			{
				for (int j = mas[0] + 1, k = mas[1] + 1; j < x, k < y; j++, k++)
					if (f[j][k]->GetColor() != 2)
						l++;
				if (l == 0)
				{
					mas[0] = x;
					mas[1] = y;
					return;
				}
			}
			if (x == mas[0] - i && y == mas[1] - i)
			{
				for (int j = mas[0] - 1, k = mas[1] - 1; j > x, k > y; j--, k--)
					if (f[j][k]->GetColor() != 2)
						l++;
				if (l == 0)
				{
					mas[0] = x;
					mas[1] = y;
					return;
				}
			}
			if (x == mas[0] + i && y == mas[1] - i)
			{
				for (int j = mas[0] + 1, k = mas[1] - 1; j < x, k > y; j++, k--)
					if (f[j][k]->GetColor() != 2)
						l++;
				if (l == 0)
				{
					mas[0] = x;
					mas[1] = y;
					return;
				}
			}
			if (x == mas[0] - i && y == mas[1] + i)
			{
				for (int j = mas[0] - 1, k = mas[1] + 1; j > x, k < y; j--, k++)
					if (f[j][k]->GetColor() != 2)
						l++;
				if (l == 0)
				{
					mas[0] = x;
					mas[1] = y;
					return;
				}
			}
		}
		if (y == mas[1])
		{
			if (x < mas[0])
				for (int i = mas[0] - 1; i > x; i--)
					if (f[i][mas[1]]->GetColor() != 2)
						l++;
			if (x > mas[0])
				for (int i = mas[0] + 1; i < x; i++)
					if (f[i][mas[1]]->GetColor() != 2)
						l++;
			if (l == 0)
			{
				mas[0] = x;
				mas[1] = y;
				return;
			}
		}
		if (x == mas[0])
		{
			if (y < mas[1])
				for (int i = mas[1] - 1; i > y; i--)
					if (f[mas[0]][i]->GetColor() != 2)
						l++;
			if (y > mas[1])
				for (int i = mas[1] + 1; i < y; i++)
					if (f[mas[0]][i]->GetColor() != 2)
						l++;
			if (l == 0)
			{
				mas[0] = x;
				mas[1] = y;
				return;
			}
		}
	}
}
void Queen::Output(int m, Graphics^ g, SolidBrush^ w, SolidBrush^ b)
{
	g->DrawString(color == 1 ? L"♕" : L"♛", gcnew System::Drawing::Font("Arial Unicode MS", 26), color == 1 ? w : b, m == 1 ? (mas[0] * 50 + 26) : ((7 - mas[0]) * 50 + 26), m == 1 ? (mas[1] * 50 + 25) : ((7 - mas[1]) * 50 + 25));
}
int Queen::GetColor()
{
	return color;
}
int Queen::GetCoordX()
{
	return mas[0];
}
int Queen::GetCoordY()
{
	return mas[1];
}
int Queen::Death()
{
	color = 2;
	return 1;
}
System::String^ Queen::GetFig()
{
	return "Queen";
}
int Queen::CheckCheck(array<array<Figure^>^>^ f)
{
	for (int i = 0; i < 8; i++)
	{
		int l = 0;
		if (f[mas[0]][i]->GetFig() == "King" && f[mas[0]][i]->GetColor() != color)
		{
			if (i < mas[1])
				for (int j = mas[1] - 1; j > i; j--)
					if (f[mas[0]][j]->GetColor() != 2)
						l++;
			if (i > mas[1])
				for (int j = mas[1] + 1; j < i; j++)
					if (f[mas[0]][j]->GetColor() != 2)
						l++;
			if (l == 0)
			{
				f[mas[0]][i]->SetCheck(1);
				return 1;
			}
		}
		if (f[i][mas[1]]->GetFig() == "King" && f[i][mas[1]]->GetColor() != color)
		{
			if (i < mas[0])
				for (int j = mas[0] - 1; j > i; j--)
					if (f[j][mas[1]]->GetColor() != 2)
						l++;
			if (i > mas[0])
				for (int j = mas[0] + 1; j < i; j++)
					if (f[j][mas[1]]->GetColor() != 2)
						l++;
			if (l == 0)
			{
				f[i][mas[1]]->SetCheck(1);
				return 1;
			}
		}
		if (mas[0] + i <= 7 && mas[1] + i <= 7)
			if (f[mas[0] + i][mas[1] + i]->GetFig() == "King" && f[mas[0] + i][mas[1] + i]->GetColor() != color)
			{
			for (int j = mas[0] + 1, k = mas[1] + 1; j < mas[0] + i, k < mas[1] + i; j++, k++)
				if (f[j][k]->GetColor() != 2)
					l++;
			if (l == 0)
			{
				f[mas[0] + i][mas[1] + i]->SetCheck(1);
				return 1;
			}
			}
		if (mas[0] - i >= 0 && mas[1] - i >= 0)
			if (f[mas[0] - i][mas[1] - i]->GetFig() == "King" && f[mas[0] - i][mas[1] - i]->GetColor() != color)
			{
			for (int j = mas[0] - 1, k = mas[1] - 1; j > mas[0] - i, k > mas[1] - i; j--, k--)
				if (f[j][k]->GetColor() != 2)
					l++;
			if (l == 0)
			{
				f[mas[0] - i][mas[1] - i]->SetCheck(1);
				return 1;
			}
			}
		if (mas[0] + i <= 7 && mas[1] - i >= 0)
			if (f[mas[0] + i][mas[1] - i]->GetFig() == "King" && f[mas[0] + i][mas[1] - i]->GetColor() != color)
			{
			for (int j = mas[0] + 1, k = mas[1] - 1; j < mas[0] + i, k > mas[1] - i; j++, k--)
				if (f[j][k]->GetColor() != 2)
					l++;
			if (l == 0)
			{
				f[mas[0] + i][mas[1] - i]->SetCheck(1);
				return 1;
			}
			}
		if (mas[0] - i >= 0 && mas[1] + i <= 7)
			if (f[mas[0] - i][mas[1] + i]->GetFig() == "King" && f[mas[0] - i][mas[1] + i]->GetColor() != color)
			{
			for (int j = mas[0] - 1, k = mas[1] + 1; j > mas[0] - i, k < mas[1] + i; j--, k++)
				if (f[j][k]->GetColor() != 2)
					l++;
			if (l == 0)
			{
				f[mas[0] - i][mas[1] + i]->SetCheck(1);
				return 1;
			}
			}
	}
	return 0;
}
void Queen::SetStats(int x, int y, int c)
{
	if (x >= 0 && y >= 0 && x <= 8 && y <= 8 && (c == 0 || c == 1))
	{
		mas[0] = x;
		mas[1] = y;
		color = c;
	}
}
int Queen::MoveCheck(array<array<Figure^>^>^ f)
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = mas[0] + 1, k = mas[1] + 1; j <= mas[0] + i, k <= mas[1] + i; j++, k++)
			if (j < 8 && k < 8 && f[j][k]->GetColor() != color)
				if (Figure::MovingCheck(mas[0], mas[1], j, k, f) == 1)
					return 1;
		for (int j = mas[0] - 1, k = mas[1] - 1; j >= mas[0] - i, k >= mas[1] - i; j--, k--)
			if (j >= 0 && k >= 0 && f[j][k]->GetColor() != color)
				if (Figure::MovingCheck(mas[0], mas[1], j, k, f) == 1)
					return 1;
		for (int j = mas[0] + 1, k = mas[1] - 1; j <= mas[0] + i, k >= mas[1] - i; j++, k--)
			if (j < 8 && k >= 0 && f[j][k]->GetColor() != color)
				if (Figure::MovingCheck(mas[0], mas[1], j, k, f) == 1)
					return 1;
		for (int j = mas[0] - 1, k = mas[1] + 1; j >= mas[0] - i, k <= mas[1] + i; j--, k++)
			if (j >= 0 && k < 8 && f[j][k]->GetColor() != color)
				if (Figure::MovingCheck(mas[0], mas[1], j, k, f) == 1)
					return 1;
		if (i < mas[1])
			for (int j = mas[1] - 1; j >= i; j--)
				if (f[mas[0]][j]->GetColor() != color)
					if (Figure::MovingCheck(mas[0], mas[1], mas[0], j, f) == 1)
						return 1;
		if (i > mas[1])
			for (int j = mas[1] + 1; j <= i; j++)
				if (f[mas[0]][j]->GetColor() != color)
					if (Figure::MovingCheck(mas[0], mas[1], mas[0], j, f) == 1)
						return 1;
		if (i < mas[0])
			for (int j = mas[0] - 1; j >= i; j--)
				if (f[j][mas[1]]->GetColor() != color)
					if (Figure::MovingCheck(mas[0], mas[1], j, mas[1], f) == 1)
						return 1;
		if (i > mas[0])
			for (int j = mas[0] + 1; j <= i; j++)
				if (f[j][mas[1]]->GetColor() != color)
					if (Figure::MovingCheck(mas[0], mas[1], j, mas[1], f) == 1)
						return 1;
	}
	return 0;
}