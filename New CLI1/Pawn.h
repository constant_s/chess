#ifndef Pawn_h
#define Pawn_h

#include "Figure.h"
#include "Queen.h"
#include "Bishop.h"
#include "Horse.h"
#include "Rook.h"
#include "DW6.h"

using namespace std;
using namespace System::Windows::Forms;
using namespace System::Drawing;
using namespace System::Runtime::Serialization;

[Serializable]

ref class Pawn : public Figure
{
public:
	Pawn();
	Pawn(Pawn%);
	void operator=(Pawn%);
	virtual void Motion(int, int, array<array<Figure^>^>^) override;
	virtual void Output(int, Graphics^, SolidBrush^, SolidBrush^) override;
	virtual int GetColor() override;
	virtual int GetCoordX() override;
	virtual int GetCoordY() override;
	virtual int Death() override;
	virtual int GetCas() override;
	virtual void SetCas() override;
	void Passant(int, int, array<array<Figure^>^>^);
	virtual System::String^ GetFig() override;
	virtual int CheckCheck(array<array<Figure^>^>^) override;
	virtual void SetStats(int, int, int) override;
	virtual void Replacement() override;
	virtual int MoveCheck(array<array<Figure^>^>^) override;
private:
	int cas;
	Figure^ q;
};

#endif