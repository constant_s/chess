﻿#include "King.h"

King::King() : Figure()
{
	cas = 0;
	check = 0;
}
King::King(King% k)
{
	mas[0] = k.mas[0];
	mas[1] = k.mas[1];
	cas = k.cas;
	check = k.check;
	color = k.color;
}

void King::operator=(King%k)
{
	mas[0] = k.mas[0];
	mas[1] = k.mas[1];
	cas = k.cas;
	check = k.check;
	color = k.color;
}

void King::Motion(int x, int y, array<array<Figure^>^>^ f)
{
	if (x >= 0 && y >= 0 && x <= 8 && y <= 8)
	{
		if ((x == mas[0] + 1 || x == mas[0] - 1 || x == mas[0]) && (y == mas[1] - 1 || y == mas[1] + 1 || y == mas[1]))
		{
			mas[0] = x;
			mas[1] = y;
			cas++;
			return;
		}
		if (mas[1] == y && ((x == mas[0] + 2 && f[x][y]->GetColor() == 2 && f[x - 1][y]->GetColor() == 2) || (x == mas[0] - 2 && f[x][y]->GetColor() == 2 && f[x + 1][y]->GetColor() == 2)) && cas == 0)
			Castling(x, f);
	}
}
void King::Output(int m, Graphics^ g, SolidBrush^ w, SolidBrush^ b)
{
	g->DrawString(color == 1 ? L"♔" : L"♚", gcnew System::Drawing::Font("Arial Unicode MS", 26), color == 1 ? w : b, m == 1 ? (mas[0] * 50 + 26) : ((7 - mas[0]) * 50 + 26), m == 1 ? (mas[1] * 50 + 25) : ((7 - mas[1]) * 50 + 25));
}
int King::GetColor()
{
	return color;
}
int King::GetCoordX()
{
	return mas[0];
}
int King::GetCoordY()
{
	return mas[1];
}
int King::Death()
{
	color = 2;
	return 0;
}
void King::Castling(int x, array<array<Figure^>^>^ f)
{
	if (x < 4)
	{
		if (f[0][mas[1]]->GetCas() == 0 && f[1][mas[1]]->GetColor() == 2 && check == 0)
		{
			f[0][mas[1]]->Motion(x + 1, mas[1], f);
				mas[0] = x;
		}
		else return;
	}
	else
	{
		if (f[7][mas[1]]->GetCas() == 0 && f[6][mas[1]]->GetColor() == 2 && check == 0)
		{
			f[7][mas[1]]->Motion(x - 1, mas[1], f);
				mas[0] = x;
		}
		else return;
	}
}
System::String^ King::GetFig()
{
	return "King";
}
int King::GetCheck()
{
	return check;
}
void King::SetCheck(int c)
{
	check = c;
}
int King::CheckCheck(array<array<Figure^>^>^ f)
{
	for (int i = -1; i < 2; i++)
		for (int j = -1; j < 2; j++)
			if (mas[0] + i >= 0 && mas[0] + i < 8 && mas[1] + j >= 0 && mas[1] + j < 8)
				if (f[mas[0] + i][mas[1] + j]->GetFig() == "King" && f[mas[0] + i][mas[1] + j]->GetColor() != color)
				{
					f[mas[0] + i][mas[1] + j]->SetCheck(1);
					return 1;
				}
	return 0;
}
void King::SetStats(int x, int y, int c)
{
	if (x >= 0 && y >= 0 && x <= 8 && y <= 8 && (c == 0 || c == 1))
	{
		mas[0] = x;
		mas[1] = y;
		color = c;
	}
}
int King::MoveCheck(array<array<Figure^>^>^ f)
{
	int l = 0; 
	for (int i = 0; i <= 2; i++)
		for (int j = 0; j <= 2; j++)
		{
		if (mas[0] + j >= 0 && mas[0] + j < 8 && mas[1] + i >= 0 && mas[1] + i < 8)
			//if (f[mas[0] + j][mas[1] + i]->GetColor() != color)
				if (Figure::MovingCheck(mas[0], mas[1], mas[0] + j, mas[1] + i, f) == 1)
					l++;
		if (mas[0] - j >= 0 && mas[0] - j < 8 && mas[1] - i >= 0 && mas[1] - i < 8)
			//if (f[mas[0] - j][mas[1] - i]->GetColor() != color)
				if (Figure::MovingCheck(mas[0], mas[1], mas[0] - j, mas[1] - i, f) == 1)
					l++;
		if (mas[0] + j >= 0 && mas[0] + j < 8 && mas[1] - i >= 0 && mas[1] - i < 8)
			//if (f[mas[0] + j][mas[1] - i]->GetColor() != color)
				if (Figure::MovingCheck(mas[0], mas[1], mas[0] + j, mas[1] - i, f) == 1)
					l++;
		if (mas[0] - j >= 0 && mas[0] - j < 8 && mas[1] + i >= 0 && mas[1] + i < 8)
			//if (f[mas[0] - j][mas[1] + i]->GetColor() != color)
				if (Figure::MovingCheck(mas[0], mas[1], mas[0] - j, mas[1] + i, f) == 1)
					l++;
		}
	if (l == 0)
		return 0;
	else
		return 1;
}