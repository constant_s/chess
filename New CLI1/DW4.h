#pragma once
#include "Field.h"

namespace New_CLI1 
{

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

public ref class DW4 : public System::Windows::Forms::Form
{
public:
	DW4(void)
	{
		n = false;
		InitializeComponent();
	}
protected:
	~DW4()
	{
		if (components)
		{
			delete components;
		}
	}
private: System::Windows::Forms::PictureBox^  pictureBox1;
private: bool n;
private: System::Windows::Forms::Button^  button1;
private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code

void InitializeComponent(void)
{
	this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
	this->button1 = (gcnew System::Windows::Forms::Button());
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
	this->SuspendLayout();
	// 
	// pictureBox1
	// 
	this->pictureBox1->BackColor = System::Drawing::Color::White;
	this->pictureBox1->Dock = System::Windows::Forms::DockStyle::Fill;
	this->pictureBox1->Location = System::Drawing::Point(0, 0);
	this->pictureBox1->Name = L"pictureBox1";
	this->pictureBox1->Size = System::Drawing::Size(353, 349);
	this->pictureBox1->TabIndex = 1;
	this->pictureBox1->TabStop = false;
	this->pictureBox1->LoadCompleted += gcnew System::ComponentModel::AsyncCompletedEventHandler(this, &DW4::pictureBox1_LoadCompleted);
	this->pictureBox1->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &DW4::pictureBox1_MouseClick);
	this->pictureBox1->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &DW4::pictureBox1_MouseMove);
	// 
	// button1
	// 
	this->button1->BackColor = System::Drawing::Color::White;
	this->button1->Font = (gcnew System::Drawing::Font(L"Segoe UI", 13.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button1->Location = System::Drawing::Point(12, 299);
	this->button1->Name = L"button1";
	this->button1->Size = System::Drawing::Size(329, 38);
	this->button1->TabIndex = 2;
	this->button1->Text = L"����� ������";
	this->button1->UseVisualStyleBackColor = false;
	this->button1->Click += gcnew System::EventHandler(this, &DW4::button1_Click);
	// 
	// DW4
	// 
	this->AutoScaleDimensions = System::Drawing::SizeF(7, 15);
	this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
	this->ClientSize = System::Drawing::Size(353, 349);
	this->ControlBox = false;
	this->Controls->Add(this->button1);
	this->Controls->Add(this->pictureBox1);
	this->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
	this->MaximizeBox = false;
	this->MinimizeBox = false;
	this->Name = L"DW4";
	this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
	this->Text = L"����������� � �������!";
	this->Load += gcnew System::EventHandler(this, &DW4::DW4_Load);
	this->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &DW4::DW4_MouseMove);
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
	this->ResumeLayout(false);
}

#pragma endregion

private: System::Void DW4_Load(System::Object^  sender, System::EventArgs^  e) 
{
	Field::Win(pictureBox1->CreateGraphics());
}
private: System::Void DW4_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (n == false)
	{
		Field::Win(pictureBox1->CreateGraphics());
		n = true;
	}
}
private: System::Void pictureBox1_LoadCompleted(System::Object^  sender, System::ComponentModel::AsyncCompletedEventArgs^  e)
{
	Field::Win(pictureBox1->CreateGraphics());
}
private: System::Void pictureBox1_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (n == false)
	{
		Field::Win(pictureBox1->CreateGraphics());
		n = true;
	}
}
private: System::Void pictureBox1_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	Field::Win(pictureBox1->CreateGraphics());
	n = true;
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->Close();
}
};
}
