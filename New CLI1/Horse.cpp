﻿#include "Horse.h"

Horse::Horse() : Figure()
{
}
Horse::Horse(Horse% h)
{
	mas[0] = h.mas[0];
	mas[1] = h.mas[1];
	color = h.color;
}

void Horse::operator=(Horse%h)
{
	mas[0] = h.mas[0];
	mas[1] = h.mas[1];
	color = h.color;
}

void Horse::Motion(int x, int y, array<array<Figure^>^>^f)
{
	if (x >= 0 && y >= 0 && x <= 8 && y <= 8)
	{
		if (y == mas[1] + 1 || y == mas[1] - 1)
			if (x == mas[0] + 2 || x == mas[0] - 2)
			{
				mas[0] = x;
				mas[1] = y;
				return;
			}
		if (y == mas[1] + 2 || y == mas[1] - 2)
			if (x == mas[0] + 1 || x == mas[0] - 1) 
			{
				mas[0] = x;
				mas[1] = y;
				return;
			}
	}
}
void Horse::Output(int m, Graphics^ g, SolidBrush^ w, SolidBrush^ b)
{
	g->DrawString(color == 1 ? L"♘" : L"♞", gcnew System::Drawing::Font("Arial Unicode MS", 26), color == 1 ? w : b, m == 1 ? (mas[0] * 50 + 26) : ((7 - mas[0]) * 50 + 26), m == 1 ? (mas[1] * 50 + 25) : ((7 - mas[1]) * 50 + 25));
}
int Horse::GetColor()
{
	return color;
}
int Horse::GetCoordX()
{
	return mas[0];
}
int Horse::GetCoordY()
{
	return mas[1];
}
int Horse::Death()
{
	color = 2;
	return 1;
}
System::String^ Horse::GetFig()
{
	return "Horse";
}
int Horse::CheckCheck(array<array<Figure^>^>^ f)
{
	if (mas[0] != 6 && mas[0] != 7 && mas[1] != 7)
		if (f[mas[0] + 2][mas[1] + 1]->GetFig() == "King" && f[mas[0] + 2][mas[1] + 1]->GetColor() != color)
		{
			f[mas[0] + 2][mas[1] + 1]->SetCheck(1);
			return 1;
		}
	if (mas[0] != 6 && mas[0] != 7 && mas[1] != 0)
		if (f[mas[0] + 2][mas[1] - 1]->GetFig() == "King" && f[mas[0] + 2][mas[1] - 1]->GetColor() != color)
		{
			f[mas[0] + 2][mas[1] - 1]->SetCheck(1);
			return 1;
		}
	if (mas[0] != 1 && mas[0] != 0 && mas[1] != 7)
		if (f[mas[0] - 2][mas[1] + 1]->GetFig() == "King" && f[mas[0] - 2][mas[1] + 1]->GetColor() != color)
		{
			f[mas[0] - 2][mas[1] + 1]->SetCheck(1);
			return 1;
		}
	if (mas[0] != 1 && mas[0] != 0 && mas[1] != 0)
		if (f[mas[0] - 2][mas[1] - 1]->GetFig() == "King" && f[mas[0] - 2][mas[1] - 1]->GetColor() != color)
		{
			f[mas[0] - 2][mas[1] - 1]->SetCheck(1);
			return 1;
		}
	if (mas[0] != 7 && mas[1] != 6 && mas[1] != 7)
		if (f[mas[0] + 1][mas[1] + 2]->GetFig() == "King" && f[mas[0] + 1][mas[1] + 2]->GetColor() != color)
		{
			f[mas[0] + 1][mas[1] + 2]->SetCheck(1);
			return 1;
		}
	if (mas[0] != 7 && mas[1] != 1 && mas[1] != 0)
		if (f[mas[0] + 1][mas[1] - 2]->GetFig() == "King" && f[mas[0] + 1][mas[1] - 2]->GetColor() != color)
		{
			f[mas[0] + 1][mas[1] - 2]->SetCheck(1);
			return 1;
		}
	if (mas[0] != 0 && mas[1] != 6 && mas[1] != 7)
		if (f[mas[0] - 1][mas[1] + 2]->GetFig() == "King" && f[mas[0] - 1][mas[1] + 2]->GetColor() != color)
		{
			f[mas[0] - 1][mas[1] + 2]->SetCheck(1);
			return 1;
		}
	if (mas[0] != 0 && mas[1] != 1 && mas[1] != 0)
		if (f[mas[0] - 1][mas[1] - 2]->GetFig() == "King" && f[mas[0] - 1][mas[1] - 2]->GetColor() != color)
		{
			f[mas[0] - 1][mas[1] - 2]->SetCheck(1);
			return 1;
		}
	return 0;
}
void Horse::SetStats(int x, int y, int c)
{
	if (x >= 0 && y >= 0 && x <= 8 && y <= 8 && (c == 0 || c == 1))
	{
		mas[0] = x;
		mas[1] = y;
		color = c;
	}
}
int Horse::MoveCheck(array<array<Figure^>^>^ f)
{
	if (mas[0] != 6 && mas[0] != 7 && mas[1] != 7)
		if (f[mas[0] + 2][mas[1] + 1]->GetColor() != color)
			if (Figure::MovingCheck(mas[0], mas[1], mas[0] + 2, mas[1] + 1, f) == 1)
				return 1;
	if (mas[0] != 6 && mas[0] != 7 && mas[1] != 0)
		if (f[mas[0] + 2][mas[1] - 1]->GetColor() != color)
			if (Figure::MovingCheck(mas[0], mas[1], mas[0] + 2, mas[1] - 1, f) == 1)
				return 1;
	if (mas[0] != 1 && mas[0] != 0 && mas[1] != 7)
		if (f[mas[0] - 2][mas[1] + 1]->GetColor() != color)
			if (Figure::MovingCheck(mas[0], mas[1], mas[0] - 2, mas[1] + 1, f) == 1)
				return 1;
	if (mas[0] != 1 && mas[0] != 0 && mas[1] != 0)
		if (f[mas[0] - 2][mas[1] - 1]->GetColor() != color)
			if (Figure::MovingCheck(mas[0], mas[1], mas[0] - 2, mas[1] - 1, f) == 1)
				return 1;
	if (mas[0] != 7 && mas[1] != 6 && mas[1] != 7)
		if (f[mas[0] + 1][mas[1] + 2]->GetColor() != color)
			if (Figure::MovingCheck(mas[0], mas[1], mas[0] + 1, mas[1] + 2, f) == 1)
				return 1;
	if (mas[0] != 7 && mas[1] != 1 && mas[1] != 0)
		if (f[mas[0] + 1][mas[1] - 2]->GetColor() != color)
			if (Figure::MovingCheck(mas[0], mas[1], mas[0] + 1, mas[1] - 2, f) == 1)
				return 1;
	if (mas[0] != 0 && mas[1] != 6 && mas[1] != 7)
		if (f[mas[0] - 1][mas[1] + 2]->GetColor() != color)
			if (Figure::MovingCheck(mas[0], mas[1], mas[0] - 1, mas[1] + 2, f) == 1)
				return 1;
	if (mas[0] != 0 && mas[1] != 1 && mas[1] != 0)
		if (f[mas[0] - 1][mas[1] - 2]->GetColor() != color)
			if (Figure::MovingCheck(mas[0], mas[1], mas[0] - 1, mas[1] - 2, f) == 1)
				return 1;
	return 0;
}