#ifndef Horse_h
#define Horse_h

#include "Figure.h"

using namespace std;
using namespace System::Windows::Forms;
using namespace System::Drawing;
using namespace System::Runtime::Serialization;

[Serializable]

ref class Horse : public Figure
{
public:
	Horse();
	Horse(Horse%);
	void operator=(Horse%);
	virtual void Motion(int, int, array<array<Figure^>^>^) override;
	virtual void Output(int, Graphics^, SolidBrush^, SolidBrush^) override;
	virtual int GetColor() override;
	virtual int GetCoordX() override;
	virtual int GetCoordY() override;
	virtual int Death() override;
	virtual System::String^ GetFig() override;
	virtual int CheckCheck(array<array<Figure^>^>^) override;
	virtual void SetStats(int, int, int) override;
	virtual int MoveCheck(array<array<Figure^>^>^) override;
};

#endif