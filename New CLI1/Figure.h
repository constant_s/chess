#ifndef Figure_h
#define Figure_h

using namespace std;
using namespace System::Windows::Forms;
using namespace System::Drawing;
using namespace System::Runtime::Serialization;
using namespace System;

[Serializable]

ref class Figure
{
public:
	Figure();
	Figure(Figure%);
	void operator=(Figure%);
	virtual void Motion(int, int, array<array<Figure^>^>^);
	virtual void Output(int, Graphics^, SolidBrush^, SolidBrush^);
	virtual int GetColor();
	virtual int GetCoordX();
	virtual int GetCoordY();
	virtual int Death();
	virtual int GetCas();
	virtual void SetCas();
	virtual System::String^ GetFig();
	virtual int GetCheck();
	virtual int CheckCheck(array<array<Figure^>^>^);
	virtual void SetCheck(int);
	virtual void SetStats(int, int, int);
	virtual int MoveCheck(array<array<Figure^>^>^);
	virtual void Replacement();
	virtual int MovingCheck(int, int, int, int, array<array<Figure^>^>^);
protected:
	cli::array<int>^ mas;
	int color;
};

#endif