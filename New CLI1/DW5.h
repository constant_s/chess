﻿#pragma once
#include "Field.h"

namespace New_CLI1
{

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

public ref class DW5 : public System::Windows::Forms::Form
{
private: SolidBrush^ wb;
private: SolidBrush^ bb;
private: SolidBrush^ wf;
private: SolidBrush^ bf;
private: SolidBrush^ c;
private: bool n;
private: System::Windows::Forms::Label^  label1;
private: System::Windows::Forms::Label^  label2;
private: System::Windows::Forms::Label^  label3;
private: System::Windows::Forms::Label^  label4;
private: System::Windows::Forms::Button^  button7;
private: System::Windows::Forms::Button^  button8;
private: System::Windows::Forms::Label^  label5;
private: System::Windows::Forms::ColorDialog^  colorDialog5;
public:
	DW5(void)
	{
		wb = gcnew SolidBrush(Color::White);
		bb = gcnew SolidBrush(Color::Gray);
		wf = gcnew SolidBrush(Color::Black);
		bf = gcnew SolidBrush(Color::Black);
		c = gcnew SolidBrush(Color::Red);
		n = false;
		InitializeComponent();
	}
protected:
	~DW5()
	{
		if (components)
		{
			delete components;
		}
	}
private: System::Windows::Forms::ColorDialog^  colorDialog1;
private: System::Windows::Forms::ColorDialog^  colorDialog2;
private: System::Windows::Forms::ColorDialog^  colorDialog3;
private: System::Windows::Forms::ColorDialog^  colorDialog4;
private: System::Windows::Forms::Button^  button1;
private: System::Windows::Forms::Button^  button2;
private: System::Windows::Forms::Button^  button3;
private: System::Windows::Forms::Button^  button4;
private: System::Windows::Forms::Button^  button5;
private: System::Windows::Forms::Button^  button6;
private: System::Windows::Forms::PictureBox^  pictureBox1;
private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code

void InitializeComponent(void)
{
	this->colorDialog1 = (gcnew System::Windows::Forms::ColorDialog());
	this->colorDialog2 = (gcnew System::Windows::Forms::ColorDialog());
	this->colorDialog3 = (gcnew System::Windows::Forms::ColorDialog());
	this->colorDialog4 = (gcnew System::Windows::Forms::ColorDialog());
	this->button1 = (gcnew System::Windows::Forms::Button());
	this->button2 = (gcnew System::Windows::Forms::Button());
	this->button3 = (gcnew System::Windows::Forms::Button());
	this->button4 = (gcnew System::Windows::Forms::Button());
	this->button5 = (gcnew System::Windows::Forms::Button());
	this->button6 = (gcnew System::Windows::Forms::Button());
	this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
	this->label1 = (gcnew System::Windows::Forms::Label());
	this->label2 = (gcnew System::Windows::Forms::Label());
	this->label3 = (gcnew System::Windows::Forms::Label());
	this->label4 = (gcnew System::Windows::Forms::Label());
	this->button7 = (gcnew System::Windows::Forms::Button());
	this->button8 = (gcnew System::Windows::Forms::Button());
	this->label5 = (gcnew System::Windows::Forms::Label());
	this->colorDialog5 = (gcnew System::Windows::Forms::ColorDialog());
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
	this->SuspendLayout();
	// 
	// button1
	// 
	this->button1->BackColor = System::Drawing::Color::White;
	this->button1->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button1->Location = System::Drawing::Point(218, 221);
	this->button1->Name = L"button1";
	this->button1->Size = System::Drawing::Size(75, 30);
	this->button1->TabIndex = 0;
	this->button1->Text = L"Выбрать";
	this->button1->UseVisualStyleBackColor = false;
	this->button1->Click += gcnew System::EventHandler(this, &DW5::button1_Click);
	// 
	// button2
	// 
	this->button2->BackColor = System::Drawing::Color::White;
	this->button2->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button2->Location = System::Drawing::Point(218, 250);
	this->button2->Name = L"button2";
	this->button2->Size = System::Drawing::Size(75, 30);
	this->button2->TabIndex = 1;
	this->button2->Text = L"Выбрать";
	this->button2->UseVisualStyleBackColor = false;
	this->button2->Click += gcnew System::EventHandler(this, &DW5::button2_Click);
	// 
	// button3
	// 
	this->button3->BackColor = System::Drawing::Color::White;
	this->button3->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button3->Location = System::Drawing::Point(218, 279);
	this->button3->Name = L"button3";
	this->button3->Size = System::Drawing::Size(75, 30);
	this->button3->TabIndex = 2;
	this->button3->Text = L"Выбрать";
	this->button3->UseVisualStyleBackColor = false;
	this->button3->Click += gcnew System::EventHandler(this, &DW5::button3_Click);
	// 
	// button4
	// 
	this->button4->BackColor = System::Drawing::Color::White;
	this->button4->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button4->Location = System::Drawing::Point(218, 308);
	this->button4->Name = L"button4";
	this->button4->Size = System::Drawing::Size(75, 30);
	this->button4->TabIndex = 3;
	this->button4->Text = L"Выбрать";
	this->button4->UseVisualStyleBackColor = false;
	this->button4->Click += gcnew System::EventHandler(this, &DW5::button4_Click);
	// 
	// button5
	// 
	this->button5->BackColor = System::Drawing::Color::White;
	this->button5->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button5->Location = System::Drawing::Point(218, 337);
	this->button5->Name = L"button5";
	this->button5->Size = System::Drawing::Size(75, 30);
	this->button5->TabIndex = 4;
	this->button5->Text = L"Выбрать";
	this->button5->UseVisualStyleBackColor = false;
	this->button5->Click += gcnew System::EventHandler(this, &DW5::button5_Click);
	// 
	// button6
	// 
	this->button6->BackColor = System::Drawing::Color::White;
	this->button6->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button6->Location = System::Drawing::Point(16, 370);
	this->button6->Name = L"button6";
	this->button6->Size = System::Drawing::Size(277, 32);
	this->button6->TabIndex = 5;
	this->button6->Text = L"Вернуть значения по умолчанию";
	this->button6->UseVisualStyleBackColor = false;
	this->button6->Click += gcnew System::EventHandler(this, &DW5::button6_Click);
	// 
	// pictureBox1
	// 
	this->pictureBox1->BackColor = System::Drawing::Color::WhiteSmoke;
	this->pictureBox1->Location = System::Drawing::Point(56, 8);
	this->pictureBox1->Name = L"pictureBox1";
	this->pictureBox1->Size = System::Drawing::Size(200, 210);
	this->pictureBox1->TabIndex = 6;
	this->pictureBox1->TabStop = false;
	this->pictureBox1->Click += gcnew System::EventHandler(this, &DW5::pictureBox1_Click);
	// 
	// label1
	// 
	this->label1->AutoSize = true;
	this->label1->BackColor = System::Drawing::Color::WhiteSmoke;
	this->label1->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->label1->Location = System::Drawing::Point(12, 228);
	this->label1->Name = L"label1";
	this->label1->Size = System::Drawing::Size(127, 19);
	this->label1->TabIndex = 7;
	this->label1->Text = L"Цвет белой клетки";
	// 
	// label2
	// 
	this->label2->AutoSize = true;
	this->label2->BackColor = System::Drawing::Color::WhiteSmoke;
	this->label2->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->label2->Location = System::Drawing::Point(12, 257);
	this->label2->Name = L"label2";
	this->label2->Size = System::Drawing::Size(136, 19);
	this->label2->TabIndex = 8;
	this->label2->Text = L"Цвет черной клетки";
	// 
	// label3
	// 
	this->label3->AutoSize = true;
	this->label3->BackColor = System::Drawing::Color::WhiteSmoke;
	this->label3->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->label3->Location = System::Drawing::Point(12, 286);
	this->label3->Name = L"label3";
	this->label3->Size = System::Drawing::Size(133, 19);
	this->label3->TabIndex = 9;
	this->label3->Text = L"Цвет белой фигуры";
	// 
	// label4
	// 
	this->label4->AutoSize = true;
	this->label4->BackColor = System::Drawing::Color::WhiteSmoke;
	this->label4->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->label4->Location = System::Drawing::Point(12, 315);
	this->label4->Name = L"label4";
	this->label4->Size = System::Drawing::Size(142, 19);
	this->label4->TabIndex = 10;
	this->label4->Text = L"Цвет черной фигуры";
	// 
	// button7
	// 
	this->button7->BackColor = System::Drawing::Color::White;
	this->button7->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button7->Location = System::Drawing::Point(17, 408);
	this->button7->Name = L"button7";
	this->button7->Size = System::Drawing::Size(131, 39);
	this->button7->TabIndex = 11;
	this->button7->Text = L"Применить";
	this->button7->UseVisualStyleBackColor = false;
	this->button7->Click += gcnew System::EventHandler(this, &DW5::button7_Click);
	// 
	// button8
	// 
	this->button8->BackColor = System::Drawing::Color::White;
	this->button8->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button8->Location = System::Drawing::Point(162, 408);
	this->button8->Name = L"button8";
	this->button8->Size = System::Drawing::Size(131, 39);
	this->button8->TabIndex = 12;
	this->button8->Text = L"Отменить";
	this->button8->UseVisualStyleBackColor = false;
	this->button8->Click += gcnew System::EventHandler(this, &DW5::button8_Click);
	// 
	// label5
	// 
	this->label5->AutoSize = true;
	this->label5->BackColor = System::Drawing::Color::WhiteSmoke;
	this->label5->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->label5->Location = System::Drawing::Point(12, 344);
	this->label5->Name = L"label5";
	this->label5->Size = System::Drawing::Size(153, 19);
	this->label5->TabIndex = 13;
	this->label5->Text = L"Цвет выбраной клетки";
	// 
	// DW5
	// 
	this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
	this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
	this->BackColor = System::Drawing::Color::WhiteSmoke;
	this->ClientSize = System::Drawing::Size(305, 459);
	this->ControlBox = false;
	this->Controls->Add(this->label5);
	this->Controls->Add(this->button8);
	this->Controls->Add(this->button7);
	this->Controls->Add(this->label4);
	this->Controls->Add(this->label3);
	this->Controls->Add(this->label2);
	this->Controls->Add(this->label1);
	this->Controls->Add(this->pictureBox1);
	this->Controls->Add(this->button6);
	this->Controls->Add(this->button5);
	this->Controls->Add(this->button4);
	this->Controls->Add(this->button3);
	this->Controls->Add(this->button2);
	this->Controls->Add(this->button1);
	this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
	this->MaximizeBox = false;
	this->MinimizeBox = false;
	this->Name = L"DW5";
	this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
	this->Text = L"Оформление";
	this->Load += gcnew System::EventHandler(this, &DW5::DW5_Load);
	this->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &DW5::DW5_MouseMove);
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
	this->ResumeLayout(false);
	this->PerformLayout();
}

#pragma endregion
		
void Draw()
{
	System::Drawing::Graphics^ g = pictureBox1->CreateGraphics();
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 2; j++)
		{
		if ((i + j) % 2 == 0)
			g->FillRectangle(bb, j * 100, i * 100, 100, 100);
		else
			g->FillRectangle(wb, j * 100, i * 100, 100, 100);
		}
	g->DrawString(L"♛", gcnew System::Drawing::Font("Arial Unicode MS", 50), bf, 0, 10);
	g->DrawString(L"♕", gcnew System::Drawing::Font("Arial Unicode MS", 50), wf, 0, 110);
	g->DrawString(L"♚", gcnew System::Drawing::Font("Arial Unicode MS", 50), bf, 100, 10);
	g->DrawString(L"♔", gcnew System::Drawing::Font("Arial Unicode MS", 50), wf, 100, 110);
	g->DrawString(L"⃞", gcnew System::Drawing::Font("Arial Unicode MS", 85), c, 73, 88);
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e)
{	
	if (colorDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		wb = gcnew SolidBrush(colorDialog1->Color);
	Draw();
}
private: System::Void pictureBox1_Click(System::Object^  sender, System::EventArgs^  e)
{
	Draw();
}
private: System::Void DW5_Load(System::Object^  sender, System::EventArgs^  e) 
{
	Draw();
}
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (colorDialog5->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		c = gcnew SolidBrush(colorDialog5->Color);
	Draw();
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (colorDialog2->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		bb = gcnew SolidBrush(colorDialog2->Color);
	Draw();
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (colorDialog3->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		wf = gcnew SolidBrush(colorDialog3->Color);
	Draw();
}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (colorDialog4->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		bf = gcnew SolidBrush(colorDialog4->Color);
	Draw();
}
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) 
{
	wb = gcnew SolidBrush(Color::White);
	bb = gcnew SolidBrush(Color::Gray);
	wf = gcnew SolidBrush(Color::Black);
	bf = gcnew SolidBrush(Color::Black);
	c = gcnew SolidBrush(Color::Red);
	Draw();
}
private: System::Void button8_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->Close();
}
private: System::Void DW5_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (n == false)
	{
		Draw();
		n = true;
	}
}
private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e) 
{
	Field::SetColors(wb, bb, wf, bf, c);
	this->DialogResult = System::Windows::Forms::DialogResult::OK;
	this->Close();
}
};
}
