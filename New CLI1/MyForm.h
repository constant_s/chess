﻿#pragma once

#include "Field.h"
#include "DW1.h"
#include "DW2.h"
#include "DW3.h"
#include "DW4.h"
#include "DW5.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>

namespace NewCLI
{

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Runtime::Serialization::Formatters::Binary;
using namespace System::Collections::Generic;
using namespace System::IO;
using namespace System::Runtime::InteropServices;

public ref class MyForm : public System::Windows::Forms::Form
{
	public:
		MyForm(void)
		{
			InitializeComponent();
			n = false;
			cl = false;
			cl_x = 10;
			cl_y = 10;
		}
	protected:
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  играToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  оПрограммеToolStripMenuItem;
	private: System::ComponentModel::Container ^components;
	private: System::Windows::Forms::ToolStripMenuItem^  начатьToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  загрузитьToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  выходToolStripMenuItem;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	private: System::Windows::Forms::ToolStripMenuItem^  настройкиToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  оформлениеToolStripMenuItem;
	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog1;
	private: System::Windows::Forms::ToolStripMenuItem^  сохранитьИгруToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  сохранитьИсториюToolStripMenuItem;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: Field ^field;
	private: bool cl;
	private: int cl_x;
	private: int cl_y;
	private: bool n;

#pragma region Windows Form Designer generated code
	
void InitializeComponent(void)
{
	this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
	this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
	this->играToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->начатьToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->сохранитьИгруToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->загрузитьToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->сохранитьИсториюToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->выходToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->настройкиToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->оформлениеToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->оПрограммеToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->textBox1 = (gcnew System::Windows::Forms::TextBox());
	this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
	this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
	this->label1 = (gcnew System::Windows::Forms::Label());
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
	this->menuStrip1->SuspendLayout();
	this->SuspendLayout();
	// 
	// pictureBox1
	// 
	this->pictureBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
		| System::Windows::Forms::AnchorStyles::Left)
		| System::Windows::Forms::AnchorStyles::Right));
	this->pictureBox1->BackColor = System::Drawing::Color::WhiteSmoke;
	this->pictureBox1->Cursor = System::Windows::Forms::Cursors::Arrow;
	this->pictureBox1->Location = System::Drawing::Point(0, 31);
	this->pictureBox1->Name = L"pictureBox1";
	this->pictureBox1->Size = System::Drawing::Size(594, 550);
	this->pictureBox1->TabIndex = 0;
	this->pictureBox1->TabStop = false;
	this->pictureBox1->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::pictureBox1_MouseDown);
	this->pictureBox1->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::pictureBox1_MouseMove);
	// 
	// menuStrip1
	// 
	this->menuStrip1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
		| System::Windows::Forms::AnchorStyles::Right));
	this->menuStrip1->AutoSize = false;
	this->menuStrip1->BackColor = System::Drawing::Color::White;
	this->menuStrip1->Dock = System::Windows::Forms::DockStyle::None;
	this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
	this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
		this->играToolStripMenuItem,
			this->настройкиToolStripMenuItem, this->оПрограммеToolStripMenuItem
	});
	this->menuStrip1->Location = System::Drawing::Point(0, 0);
	this->menuStrip1->Name = L"menuStrip1";
	this->menuStrip1->Size = System::Drawing::Size(923, 28);
	this->menuStrip1->TabIndex = 4;
	this->menuStrip1->Text = L"menuStrip1";
	// 
	// играToolStripMenuItem
	// 
	this->играToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {
		this->начатьToolStripMenuItem,
			this->сохранитьИгруToolStripMenuItem, this->загрузитьToolStripMenuItem, this->сохранитьИсториюToolStripMenuItem, this->выходToolStripMenuItem
	});
	this->играToolStripMenuItem->Font = (gcnew System::Drawing::Font(L"Segoe UI", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->играToolStripMenuItem->Name = L"играToolStripMenuItem";
	this->играToolStripMenuItem->Size = System::Drawing::Size(55, 24);
	this->играToolStripMenuItem->Text = L"Игра";
	// 
	// начатьToolStripMenuItem
	// 
	this->начатьToolStripMenuItem->BackColor = System::Drawing::Color::White;
	this->начатьToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
	this->начатьToolStripMenuItem->Font = (gcnew System::Drawing::Font(L"Segoe UI", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->начатьToolStripMenuItem->Name = L"начатьToolStripMenuItem";
	this->начатьToolStripMenuItem->Size = System::Drawing::Size(225, 26);
	this->начатьToolStripMenuItem->Text = L"Начать Новую Игру";
	this->начатьToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::начатьToolStripMenuItem_Click);
	// 
	// сохранитьИгруToolStripMenuItem
	// 
	this->сохранитьИгруToolStripMenuItem->BackColor = System::Drawing::Color::White;
	this->сохранитьИгруToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
	this->сохранитьИгруToolStripMenuItem->Name = L"сохранитьИгруToolStripMenuItem";
	this->сохранитьИгруToolStripMenuItem->Size = System::Drawing::Size(225, 26);
	this->сохранитьИгруToolStripMenuItem->Text = L"Сохранить Игру";
	this->сохранитьИгруToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::сохранитьИгруToolStripMenuItem_Click);
	// 
	// загрузитьToolStripMenuItem
	// 
	this->загрузитьToolStripMenuItem->BackColor = System::Drawing::Color::White;
	this->загрузитьToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
	this->загрузитьToolStripMenuItem->Font = (gcnew System::Drawing::Font(L"Segoe UI", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->загрузитьToolStripMenuItem->Name = L"загрузитьToolStripMenuItem";
	this->загрузитьToolStripMenuItem->Size = System::Drawing::Size(225, 26);
	this->загрузитьToolStripMenuItem->Text = L"Загрузить Игру";
	this->загрузитьToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::загрузитьToolStripMenuItem_Click);
	// 
	// сохранитьИсториюToolStripMenuItem
	// 
	this->сохранитьИсториюToolStripMenuItem->BackColor = System::Drawing::Color::White;
	this->сохранитьИсториюToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
	this->сохранитьИсториюToolStripMenuItem->Name = L"сохранитьИсториюToolStripMenuItem";
	this->сохранитьИсториюToolStripMenuItem->Size = System::Drawing::Size(225, 26);
	this->сохранитьИсториюToolStripMenuItem->Text = L"Сохранить Историю";
	this->сохранитьИсториюToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::сохранитьИсториюToolStripMenuItem_Click);
	// 
	// выходToolStripMenuItem
	// 
	this->выходToolStripMenuItem->BackColor = System::Drawing::Color::White;
	this->выходToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
	this->выходToolStripMenuItem->Name = L"выходToolStripMenuItem";
	this->выходToolStripMenuItem->Size = System::Drawing::Size(225, 26);
	this->выходToolStripMenuItem->Text = L"Выход";
	this->выходToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::выходToolStripMenuItem_Click);
	// 
	// настройкиToolStripMenuItem
	// 
	this->настройкиToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->оформлениеToolStripMenuItem });
	this->настройкиToolStripMenuItem->Name = L"настройкиToolStripMenuItem";
	this->настройкиToolStripMenuItem->Size = System::Drawing::Size(96, 24);
	this->настройкиToolStripMenuItem->Text = L"Настройки";
	// 
	// оформлениеToolStripMenuItem
	// 
	this->оформлениеToolStripMenuItem->BackColor = System::Drawing::Color::White;
	this->оформлениеToolStripMenuItem->Name = L"оформлениеToolStripMenuItem";
	this->оформлениеToolStripMenuItem->Size = System::Drawing::Size(176, 26);
	this->оформлениеToolStripMenuItem->Text = L"Оформление";
	this->оформлениеToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::оформлениеToolStripMenuItem_Click);
	// 
	// оПрограммеToolStripMenuItem
	// 
	this->оПрограммеToolStripMenuItem->Font = (gcnew System::Drawing::Font(L"Segoe UI", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->оПрограммеToolStripMenuItem->Name = L"оПрограммеToolStripMenuItem";
	this->оПрограммеToolStripMenuItem->Size = System::Drawing::Size(118, 24);
	this->оПрограммеToolStripMenuItem->Text = L"О Программе";
	this->оПрограммеToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::оПрограммеToolStripMenuItem_Click);
	// 
	// textBox1
	// 
	this->textBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
		| System::Windows::Forms::AnchorStyles::Right));
	this->textBox1->BackColor = System::Drawing::Color::White;
	this->textBox1->Cursor = System::Windows::Forms::Cursors::IBeam;
	this->textBox1->Font = (gcnew System::Drawing::Font(L"Segoe UI", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(0)));
	this->textBox1->Location = System::Drawing::Point(594, 53);
	this->textBox1->Multiline = true;
	this->textBox1->Name = L"textBox1";
	this->textBox1->ReadOnly = true;
	this->textBox1->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
	this->textBox1->ShortcutsEnabled = false;
	this->textBox1->Size = System::Drawing::Size(232, 516);
	this->textBox1->TabIndex = 11;
	this->textBox1->TabStop = false;
	this->textBox1->Text = L"Welcome, Dear Friend!!! ";
	this->textBox1->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
	this->textBox1->WordWrap = false;
	this->textBox1->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox1_TextChanged);
	// 
	// openFileDialog1
	// 
	this->openFileDialog1->FileName = L"openFileDialog1";
	// 
	// label1
	// 
	this->label1->AutoSize = true;
	this->label1->Font = (gcnew System::Drawing::Font(L"Segoe UI", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->label1->Location = System::Drawing::Point(590, 27);
	this->label1->Name = L"label1";
	this->label1->Size = System::Drawing::Size(142, 23);
	this->label1->TabIndex = 12;
	this->label1->Text = L"История партии:";
	// 
	// MyForm
	// 
	this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
	this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
	this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
	this->BackColor = System::Drawing::Color::WhiteSmoke;
	this->ClientSize = System::Drawing::Size(837, 581);
	this->Controls->Add(this->textBox1);
	this->Controls->Add(this->pictureBox1);
	this->Controls->Add(this->menuStrip1);
	this->Controls->Add(this->label1);
	this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
	this->MainMenuStrip = this->menuStrip1;
	this->MaximizeBox = false;
	this->MinimizeBox = false;
	this->Name = L"MyForm";
	this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
	this->Text = L"♘ Chess ♘";
	this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
	this->menuStrip1->ResumeLayout(false);
	this->menuStrip1->PerformLayout();
	this->ResumeLayout(false);
	this->PerformLayout();

}

#pragma endregion

private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e)
{
	Field::About(pictureBox1->CreateGraphics());
}
private: System::Void начатьToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (Field::GetI() == 1)
	{
		New_CLI1::DW1^ d = gcnew New_CLI1::DW1();
		if (d->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			field = gcnew Field;
			textBox1->Clear();
			field->OutField(pictureBox1->CreateGraphics());
		}
	}
	else
	{
		field = gcnew Field;
		textBox1->Clear();
		field->OutField(pictureBox1->CreateGraphics());
	}
}
private: System::Void оПрограммеToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	New_CLI1::DW3^ d = gcnew New_CLI1::DW3();
	d->ShowDialog();
}
private: System::Void pictureBox1_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (Field::GetI() != 1 && n == false)
	{
		Field::About(pictureBox1->CreateGraphics());
		n = true;
	}
}
private: System::Void выходToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (Field::GetI() == 1)
	{
		New_CLI1::DW2^ d = gcnew New_CLI1::DW2();
		if (d->ShowDialog() == System::Windows::Forms::DialogResult::OK)
			Application::Exit();
	}
	else Application::Exit();
}
private: System::Void загрузитьToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{	
		field = gcnew Field();
		field = field->LoadFromFile(openFileDialog1->FileName);
		textBox1->Clear();
		textBox1->Text += field->GetString() + "Loading Successful.\r\n";
		field->OutField(pictureBox1->CreateGraphics());
	}
}
private: System::Void сохранитьИсториюToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (field->GetI() == 1 && saveFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		char* str1 = (char*)(void*)Marshal::StringToHGlobalAnsi(field->GetString());
		char* str2 = (char*)(void*)Marshal::StringToHGlobalAnsi(saveFileDialog1->FileName);
		ofstream Out(str2, ios::out);
		Out << str1;
		textBox1->Text += "Saving Story Successful.\r\n";
	}
	else
		textBox1->Text += "Saving Story Failed.\r\n";
}
private: System::Void сохранитьИгруToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (field->GetI() == 1 && saveFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		field->SaveToFile(field, saveFileDialog1->FileName);
		textBox1->Text += "Saving Game Successful.\r\n";
	}
	else
		textBox1->Text += "Saving Game Failed.\r\n";
}
private: System::Void pictureBox1_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (Field::GetI() == 1)
	{
		System::Drawing::Graphics^ g = pictureBox1->CreateGraphics();
		if (cl == false)
		{
			if (e->Location.X >= 425)
				cl_x = (e->Location.X - 75) / 50;
			else
				cl_x = (e->Location.X - 25) / 50;
			if (e->Location.Y >= 425)
				cl_y = (e->Location.Y - 75) / 50;
			else
				cl_y = (e->Location.Y - 25) / 50;
			g->DrawString(L"⃞", gcnew System::Drawing::Font("Arial Unicode MS", 45), field->GetCon(), cl_x * 50 + 10, cl_y * 50 + 12);
			cl = true;
		}
		else
		{
			if (cl_x == ((e->Location.X - 25) / 50) && cl_y == ((e->Location.Y - 25) / 50))
			{
				cl = false;
				field->OutField(g);
			}
			else
			{
				if (field->GetV() % 2 == 0)
				{
					cl_x = 7 - cl_x;
					cl_y = 7 - cl_y;
				}
				if (cl_x >= 0 && cl_y >= 0 && cl_x < 8 && cl_y < 8 && field->GetColor(cl_x, cl_y) != 2 && field->GetWin() == 2)
				{
					int x;
					int y;
					if (e->Location.X >= 425)
						x = (e->Location.X - 75) / 50;
					else
						x = (e->Location.X - 25) / 50;
					if (e->Location.Y >= 425)
						y = (e->Location.Y - 75) / 50;
					else
						y = (e->Location.Y - 25) / 50;
					if (field->GetV() % 2 == 0)
					{
						x = 7 - x;
						y = 7 - y;
					}
					int r = field->Move(cl_x, cl_y, x, y, textBox1);
					if (r > 1 || r == 0)
					{
						textBox1->Clear();
						field->SetString((field->GetV() - 1) + " : " + ((field->GetV() % 2) ? "W " : "B ") + field->GetFigure(x, y) + " " + wchar_t((char*)cl_x + 65) + (8 - cl_y) + " - " + wchar_t((char*)x + 65) + (8 - y) + "\t\t\n");
						textBox1->Text += field->GetString();
						if (r == 2 || r == 3)
						{
							New_CLI1::DW4^ d = gcnew New_CLI1::DW4();
							d->ShowDialog();
							field->OutField(g);
						}
						else
						{
							field->OutField(g);
							cl_x = 10;
							cl_y = 10;
							cl = false;
						}
					}
					else
					{
						field->OutField(g);
						cl_x = 10;
						cl_y = 10;
						cl = false;
					}
				}
				else
				{
					field->OutField(g);
					if (e->Location.X >= 425)
						cl_x = (e->Location.X - 75) / 50;
					else
						cl_x = (e->Location.X - 25) / 50;
					if (e->Location.Y >= 425)
						cl_y = (e->Location.Y - 75) / 50;
					else
						cl_y = (e->Location.Y - 25) / 50;
					g->DrawString(L"⃞", gcnew System::Drawing::Font("Arial Unicode MS", 45), field->GetCon(), cl_x * 50 + 10, cl_y * 50 + 12);
					cl = true;
				}
			}
		}
	}
}
private: System::Void оформлениеToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	New_CLI1::DW5^ d = gcnew New_CLI1::DW5();
	if (d->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		if(field->GetI() == 1)
			field->OutField(pictureBox1->CreateGraphics());
		else
			Field::SetI(2);
}
private: System::Void textBox1_TextChanged(System::Object^  sender, System::EventArgs^  e)
{
	textBox1->SelectionStart = textBox1->Text->Length;
	textBox1->ScrollToCaret();
}
};
}