#ifndef Field_h
#define Field_h

#include "Figure.h"
#include "Pawn.h"
#include "Horse.h"
#include "Rook.h"
#include "Bishop.h"
#include "King.h"
#include "Queen.h"

using namespace std;
using namespace System::Windows::Forms;
using namespace System::Drawing;
using namespace System::Runtime::Serialization;
using namespace System::IO;
using namespace System;

[Serializable]

ref class Field
{
private:
	static int win;
	static int i;
	static SolidBrush^ wb;
	static SolidBrush^ bb;
	static SolidBrush^ wf;
	static SolidBrush^ bf;
	static SolidBrush^ con;
	int v;
	System::String^ s;
	array<array<Figure^>^>^  field;
	Figure g;
	Pawn p00, p01, p02, p03, p04, p05, p06, p07;
	Pawn p10, p11, p12, p13, p14, p15, p16, p17;
	Horse h01, h02, h11, h12;
	Rook r00, r01, r10, r11;
	Bishop b01, b02, b11, b12;
	Queen q0, q1;
	King k0, k1;
public:
	Field();
	~Field();
	void operator=(Field^);
	void Entry();
	void Entry(Figure^);
	void OutField(Graphics^);
	int Move(int,int,int,int,TextBox^);
	static void About(Graphics^);
	static void Win(Graphics^);
	System::String^ GetFigure(int, int);
	static int GetI();
	static void SetI(int);
	int GetV();
	void SetString(System::String^);
	System::String^ GetString();
	static void SetWin(int);
	static int GetWin();
	void SaveToFile(System::Object^ obj, System::String ^s);
	Field^ Field::LoadFromFile(System::String^ s);
	static void SetColors(SolidBrush^, SolidBrush^, SolidBrush^, SolidBrush^, SolidBrush^);
	SolidBrush^ GetCon();
	int GetColor(int, int);
	int Patta();
};

#endif
