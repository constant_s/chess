﻿#include "Pawn.h"

Pawn::Pawn() : Figure()
{
	cas = 0;
	q = gcnew Figure();
}
Pawn::Pawn(Pawn %p)
{
	mas[0] = p.mas[0];
	mas[1] = p.mas[1];
	color = p.color;
	cas = p.cas;
}

void Pawn::operator=(Pawn%p)
{
	mas[0] = p.mas[0];
	mas[1] = p.mas[1];
	color = p.color;
	cas = p.cas;
	q = p.q;
}

void Pawn::Motion(int x, int y, array<array<Figure^>^>^ f)
{
	int i;
	if (color == 0) i = 1;
	else i = -1;
	if (q->GetColor() == 2)
	{
		if (x >= 0 && y >= 0 && x <= 8 && y <= 8)
		{
			if (x == mas[0] && y == mas[1] + i)
				if (f[x][y]->GetColor() == 2)
				{
				mas[0] = x;
				mas[1] = y;
				cas += 2;
				Replacement();
				return;
				}
			if ((x == mas[0] + 1 || x == mas[0] - 1) && y == mas[1] + i)
			{
				if (f[x][y - i]->GetCas() == 1)
					Passant(x, y, f);
				if (color == 0)
				{
					if (f[x][y]->GetColor() == 1)
					{
						mas[0] = x;
						mas[1] = y;
						cas += 2;
						Replacement();
						return;
					}
				}
				else
					if (f[x][y]->GetColor() == 0)
					{
					mas[0] = x;
					mas[1] = y;
					cas += 2;
					Replacement();
					return;
					}
			}
			if (x == mas[0] && y == mas[1] + 2 * i)
				if (f[x][y]->GetColor() == 2 && f[x][y - i]->GetColor() == 2)
				{
				if (color == 0)
				{
					if (mas[1] == 1)
					{
						mas[0] = x;
						mas[1] = y;
						cas++;
						return;
					}
				}
				else
					if (mas[1] == 6)
					{
					mas[0] = x;
					mas[1] = y;
					cas++;
					return;
					}
				}
		}
	}
	else q->Motion(x, y, f);
}
void Pawn::Output(int m, Graphics^ g, SolidBrush^ w, SolidBrush^ b)
{
	if (q->GetColor() == 2)
		g->DrawString(color == 1 ? L"♙" : L"♟", gcnew System::Drawing::Font("Arial Unicode MS", 26), color == 1 ? w : b, m == 1 ? (mas[0] * 50 + 26) : ((7 - mas[0]) * 50 + 26), m == 1 ? (mas[1] * 50 + 25) : ((7 - mas[1]) * 50 + 25));
	else q->Output(m, g, w, b);
}
int Pawn::GetColor()
{
	return color;
}
int Pawn::GetCoordX()
{
	if (q->GetColor() == 2)
		return mas[0];
	else return q->GetCoordX();
}
int Pawn::GetCoordY()
{
	if (q->GetColor() == 2)
		return mas[1];
	else return q->GetCoordY();
}
int Pawn::Death()
{
	color = 2;
	return q->Death();
}
void Pawn::Passant(int x, int y, array<array<Figure^>^>^ f)
{
	int i;
	if (color == 0) i = 1;
	else i = -1;
	if (f[x][y - i]->GetColor() != color)
	{
		f[x][y - i]->Death();
		mas[0] = x;
		mas[1] = y;
	}
}
int Pawn::GetCas()
{
	return cas;
}
void Pawn::SetCas()
{
	cas++;
}
System::String^ Pawn::GetFig()
{
	if (q->GetColor() == 2)
		return "Pawn";
	else return q->GetFig();
}
int Pawn::CheckCheck(array<array<Figure^>^>^ f)
{
	if (q->GetColor() == 2)
	{
		if (color == 0)
		{
			if (mas[0] < 7 && mas[1] < 7)
				if (f[mas[0] + 1][mas[1] + 1]->GetFig() == "King" && f[mas[0] + 1][mas[1] + 1]->GetColor() == 1)
				{
				f[mas[0] + 1][mas[1] + 1]->SetCheck(1);
				return 1;
				}
			if (mas[0] > 0 && mas[1] < 7)
				if (f[mas[0] - 1][mas[1] + 1]->GetFig() == "King" && f[mas[0] - 1][mas[1] + 1]->GetColor() == 1)
				{
				f[mas[0] - 1][mas[1] + 1]->SetCheck(1);
				return 1;
				}
		}
		if (color == 1)
		{
			if (mas[0] < 7 && mas[1] > 0)
				if (f[mas[0] + 1][mas[1] - 1]->GetFig() == "King" && f[mas[0] + 1][mas[1] - 1]->GetColor() == 0)
				{
				f[mas[0] + 1][mas[1] - 1]->SetCheck(1);
				return 1;
				}
			if (mas[0] > 0 && mas[1] > 0)
				if (f[mas[0] - 1][mas[1] - 1]->GetFig() == "King" && f[mas[0] - 1][mas[1] - 1]->GetColor() == 0)
				{
				f[mas[0] - 1][mas[1] - 1]->SetCheck(1);
				return 1;
				}
		}
	}
	else return q->CheckCheck(f);
	return 0;
}
void Pawn::SetStats(int x, int y, int c)
{
	if (q->GetColor() == 2)
	{
		if (x >= 0 && y >= 0 && x <= 8 && y <= 8 && (c == 0 || c == 1))
		{
			mas[0] = x;
			mas[1] = y;
			color = c;
		}
	}
	else q->SetStats(x, y, c);
}
void Pawn::Replacement()
{
	if ((color == 0 && mas[1] == 7) || (color == 1 && mas[1] == 0))
	{
		New_CLI1::DW6^ d = gcnew New_CLI1::DW6(color);
		System::Windows::Forms::DialogResult res;
		if (cas > 10)
		{
			if (cas == 11)
				q = gcnew Queen;
			if (cas == 12)
				q = gcnew Bishop;
			if (cas == 13)
				q = gcnew Horse;
			if (cas == 14)
				q = gcnew Rook;
		}
		else
		{
			res = d->ShowDialog();
			if (res == System::Windows::Forms::DialogResult::OK)
			{
				q = gcnew Queen;
				cas = 11;
			}
			else
				if (res == System::Windows::Forms::DialogResult::Yes)
				{
					q = gcnew Bishop;
					cas = 12;
				}
				else
					if (res == System::Windows::Forms::DialogResult::No)
					{
						q = gcnew Horse;
						cas = 13;
					}
					else
						if (res == System::Windows::Forms::DialogResult::Cancel)
						{
							q = gcnew Rook;
							cas = 14;
						}
		}
		q->SetStats(mas[0], mas[1], color);
	}

}
int Pawn::MoveCheck(array<array<Figure^>^>^ f)
{
	if (q->GetColor() == 2)
	{
		if (color == 0)
		{
			if (mas[0] < 7 && mas[1] < 7)
				if (f[mas[0] + 1][mas[1] + 1]->GetColor() == 1)
					if (Figure::MovingCheck(mas[0], mas[1], mas[0] + 1, mas[1] + 1, f) == 1)
						return 1;
			if (mas[0] > 0 && mas[1] < 7)
				if (f[mas[0] - 1][mas[1] + 1]->GetColor() == 1)
					if (Figure::MovingCheck(mas[0], mas[1], mas[0] - 1, mas[1] + 1, f) == 1)
						return 1;
		}
		if (color == 1)
		{
			if (mas[0] < 7 && mas[1] > 0)
				if (f[mas[0] + 1][mas[1] - 1]->GetColor() == 0)
					if (Figure::MovingCheck(mas[0], mas[1], mas[0] + 1, mas[1] - 1, f) == 1)
						return 1;
			if (mas[0] > 0 && mas[1] > 0)
				if (f[mas[0] - 1][mas[1] - 1]->GetColor() == 0)
					if (Figure::MovingCheck(mas[0], mas[1], mas[0] - 1, mas[1] - 1, f) == 1)
						return 1;
		}
		int i;
		if (color == 0) i = 1;
		else i = -1;
		if (mas[1] + i >= 0 && mas[1] + i < 8)
			if (f[mas[0]][mas[1] + i]->GetColor() == 2)
				if (Figure::MovingCheck(mas[0], mas[1], mas[0], mas[1] + i, f) == 1)
					return 1;
		if ((mas[0] + 1 < 8 || mas[0] - 1 >= 0) && mas[1] + i < 8 && mas[1] + i >= 0)
		{
			if ((mas[0] < 7 && mas[1] < 7 && f[mas[0] + 1][mas[1] + i]->GetCas() == 1) || (mas[0] > 0 && mas[1] < 7 && f[mas[0] - 1][mas[1] + i]->GetCas() == 1))
				if (color == 0)
				{
				if (f[mas[0] + 1][mas[1] + i]->GetColor() == 1)
					if (Figure::MovingCheck(mas[0], mas[1], mas[0] + 1, mas[1] + i, f) == 1)
						return 1;
				if (f[mas[0] - 1][mas[1] + i]->GetColor() == 1)
					if (Figure::MovingCheck(mas[0], mas[1], mas[0] - 1, mas[1] + i, f) == 1)
						return 1;
				}
				else
				{
					if (f[mas[0] + 1][mas[1] + i]->GetColor() == 0)
						if (Figure::MovingCheck(mas[0], mas[1], mas[0] + 1, mas[1] + i, f) == 1)
							return 1;
					if (f[mas[0] - 1][mas[1] + i]->GetColor() == 0)
						if (Figure::MovingCheck(mas[0], mas[1], mas[0] - 1, mas[1] + i, f) == 1)
							return 1;
				}
		}
		if (mas[1] + 2 * i >= 0 && mas[1] + 2 * i < 8)
			if (f[mas[0]][mas[1] + 2 * i]->GetColor() == 2 && f[mas[0]][mas[1] + i]->GetColor() == 2)
			{
			if (color == 0)
				if (mas[1] == 1)
					if (Figure::MovingCheck(mas[0], mas[1], mas[0], mas[1] + 2 * i, f) == 1)
						return 1;
					else
						if (mas[1] == 6)
							if (Figure::MovingCheck(mas[0], mas[1], mas[0], mas[1] + 2 * i, f) == 1)
								return 1;
			}
	}
	else return q->MoveCheck(f);
	return 0;
}