#pragma once
#include "Field.h"

namespace New_CLI1 
{

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

public ref class DW3 : public System::Windows::Forms::Form
{
public:
	DW3(void)
	{
		n = false;
		InitializeComponent();
	}
protected:
	~DW3()
	{
		if (components)
		{
			delete components;
		}
	}
private: System::Windows::Forms::PictureBox^  pictureBox1;
private: bool n;
private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code

void InitializeComponent(void)
{
	this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
	this->SuspendLayout();
	// 
	// pictureBox1
	// 
	this->pictureBox1->BackColor = System::Drawing::Color::White;
	this->pictureBox1->Dock = System::Windows::Forms::DockStyle::Fill;
	this->pictureBox1->Location = System::Drawing::Point(0, 0);
	this->pictureBox1->Name = L"pictureBox1";
	this->pictureBox1->Size = System::Drawing::Size(578, 549);
	this->pictureBox1->TabIndex = 0;
	this->pictureBox1->TabStop = false;
	this->pictureBox1->LoadCompleted += gcnew System::ComponentModel::AsyncCompletedEventHandler(this, &DW3::pictureBox1_LoadCompleted);
	this->pictureBox1->Click += gcnew System::EventHandler(this, &DW3::pictureBox1_Click);
	this->pictureBox1->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &DW3::pictureBox1_MouseMove);
	// 
	// DW3
	// 
	this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
	this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
	this->ClientSize = System::Drawing::Size(578, 549);
	this->ControlBox = false;
	this->Controls->Add(this->pictureBox1);
	this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
	this->MaximizeBox = false;
	this->MinimizeBox = false;
	this->Name = L"DW3";
	this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
	this->Text = L"� ���������";
	this->Load += gcnew System::EventHandler(this, &DW3::DW3_Load);
	this->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &DW3::DW3_MouseMove);
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
	this->ResumeLayout(false);
}

#pragma endregion

private: System::Void pictureBox1_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->Close();
}
private: System::Void pictureBox1_LoadCompleted(System::Object^  sender, System::ComponentModel::AsyncCompletedEventArgs^  e)
{
	System::Drawing::Graphics^ gr = pictureBox1->CreateGraphics();
	Field::About(gr);
}
private: System::Void pictureBox1_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) 
{
	if (n == false)
	{
		Field::About(pictureBox1->CreateGraphics());
		n = true;
	}
}
private: System::Void DW3_Load(System::Object^  sender, System::EventArgs^  e)
{
	Field::About(pictureBox1->CreateGraphics());
}
private: System::Void DW3_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) 
{
	if (n == false)
	{
		Field::About(pictureBox1->CreateGraphics());
		n = true;
	}
}
};
}
